import 'package:flutter/material.dart';
import '../constants.dart';

class GenderCardProps extends StatelessWidget {
  final dynamic iconName;
  final dynamic sexText;

  const GenderCardProps({this.iconName, this.sexText});

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Icon(
          iconName,
          size: 80.0,
        ),
        SizedBox(
          height: 15.0,
        ),
        Text(
          sexText,
          style: kLabelTextStyle,
        )
      ],
    );
  }
}
