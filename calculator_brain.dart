import 'dart:math';

class BMICalculator {
  BMICalculator({this.height, this.weight});

  final height;
  final weight;

  late double bmi;

  String calculate_bmi() {
    bmi = weight / pow(height / 100, 2);
    return bmi.toStringAsFixed(1);
  }

  String getResult() {
    if (bmi >= 25) {
      return 'Overweight';
    } else if (bmi > 18.5) {
      return 'Normal';
    } else {
      return 'Underweight';
    }
  }

  String getInterpretation() {
    if (bmi >= 25) {
      return 'You are at a heavier weight than average for your height';
    } else if (bmi > 18.5) {
      return 'You are at a healthy and average ratio';
    } else {
      return 'You are underweight for your height';
    }
  }
}
