import 'package:flutter/material.dart';

const kActiveCardColor = Color(0xFF1D1E33);
const kInactiveCardColor = Color(0xFF111328);
const kBottomCardColor = Color(0xFFEB1555);

const kLabelTextStyle = TextStyle(
  fontSize: 20.0,
  color: Color(0xFF8D8E98),
);

const kNumberTextStyle = TextStyle(
  fontSize: 50,
  fontWeight: FontWeight.w900,
);

const kLargeTextStyle = TextStyle(
  fontSize: 30,
  fontWeight: FontWeight.w900,
);
